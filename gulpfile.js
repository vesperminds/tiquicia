/*jslint node: true */

'use strict';

var THEME = 'wp-content/themes/tiquicia-v1';
var DIST = THEME + '/dist';

var PATHS = {

	build: {

		stylesheets: [
			THEME + '/assets/stylesheets/**/*.scss',
			'!' + THEME + '/assets/stylesheets/**/_*.scss'
		]

	},

	watch: {

		stylesheets: [
			THEME + '/assets/stylesheets/**/*.scss'
		]

	},

	dist: {
		css: DIST + '/css'
	}

};

var BROWSERLIST = '> 1%, last 3 versions, IE 8, IE 9, Firefox ESR';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var postcss = require('gulp-postcss');
var del = require('del');
var autoprefixer = require('autoprefixer');
var pixrem = require('pixrem');

gulp.task('clean', function() {
	return del([ DIST ]);
});

gulp.task('sass', ['clean'], function() {

	var onError = function(err) {

		var msg = err.message;
		msg = msg.replace(THEME + '/assets/stylesheets/', '');

		notify.onError({
			title: 'SASS',
			subtitle: 'Build error',
			message: msg,
			sound: false
		})(err);

		this.emit('end');

	};

	var processors = [
		pixrem(undefined, {
			browsers: BROWSERLIST
		}),
		autoprefixer({
			browsers: BROWSERLIST
		})
	];

	gulp.src(PATHS.build.stylesheets)
	.pipe(sourcemaps.init())
	.pipe(plumber({ errorHandler: onError }))
	.pipe(sass({
		outputStyle: 'expanded'
	}).on('error', sass.logError))
	.pipe(postcss(processors))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(PATHS.dist.css));

});

gulp.task('watch', function() {

	gulp.watch(PATHS.watch.stylesheets, [ 'sass' ]);

});

gulp.task('default', [ 'watch', 'sass' ]);
