# Tiquicia Tours #

## Setup ##

Settings > General:

* Change tagline
* Change timezone to Costa Rica

Settings > Permalink

* Chage to Custom Structure: %postname%-%post_id%

Pages:

* Add new page to be used as home page
* Add new page to be used as blog index (articles)

Settings > Reading

* Set Front page displays to "A static page", and select both pages that were created previously

Appearance > Themes

* Select Tiquicia Tours v1 as current theme

Other pages:

* Add new page to be used as tours index (tours)
* Add new page to be used as gallery index (gallery), and set the headline and featured image
* Add new page to be used as about us (about-us), paste content, and set the headline and featured image
