<?php

	$ppp = 24;
	$ajax = !empty($_GET['ajax']);
	$page = !empty($_GET['_page']) & is_numeric($_GET['_page']) ? (int) $_GET['_page'] : null;

	if (!empty($page) && $page < 1) {
		$page = 1;
	}

	$opts = [
		'type' => 'photo',
		'order' => 'DESC',
		'orderby' => 'date',
		'limit' => $ppp,
		'page' => $page,
		'thumbsize' => 'tiquicia-post-thumbnail',
		'extraimage' => 'tiquicia-gallery-image'
	];

	$photos = vp_get_all($opts);

	$has_more = count($photos) >= $ppp;

?>

<?php if (!$ajax): ?>

<?php get_template_part('parts/html-header'); ?>
<?php get_template_part('parts/site-nav'); ?>

<?php endif; ?>

<?php while (have_posts()): the_post(); ?>

<?php if (!$ajax): ?>

<?php
	$thumbnail_id = get_post_thumbnail_id();
	$image = wp_get_attachment_image_src($thumbnail_id, 'tiquicia-post-thumbnail-cover');
	$headline = get_post_meta(get_the_ID(), 'headline', true);
?>

<?php if (!empty($image)): ?>
<div class="page-single" eg-grid="loose-container">

	<div eg-grid="row">

		<section class="hero" eg-grid="col-12"
		style="background-image: url(<?= $image[0] ?>)">

			<div class="hero--content hero--content-centered">

				<h2>
					<strong>Welcome to</strong><br>
					Tiquicia Tours Puntarenas
				</h2>

				<?php if (!empty($headline)): ?>
				<p>
					<?= $headline ?>
				</p>
				<?php endif; ?>

			</div>

		</section>

	</div>

</div>
<?php endif; ?>

<div class="page-single page--section" eg-grid="container">
	<div eg-grid="row"><div eg-grid="col-10 wrap-1 col-12@tablet wrap-0@tablet col-12@mobile wrap-0@mobile">

		<div eg-grid="row">

			<section class="copy copy--light copy--centered" eg-grid="col-12">

				<h2><u><?= get_the_title(); ?></u></h2>

				<p>
					Simply click on the thumbnail to preview a slide gallery of
					the images and navigate through them.
				</p>

				<p>
					Every image has a story to tell!
				</p>

			</section>

			<section class="gallery gallery--ajax-wrapper page--section" eg-grid="col-12">

<?php endif; ?>

				<div eg-grid="row align-center" data-has-more="<?= $has_more ? 1 : 0 ?>">
				<?php foreach ($photos as $photo): ?>

					<a href="<?= $photo['extraimage'] ?: '#' ?>" class="gallery--element" eg-lightbox="eg-lightbox"
					eg-grid="col-3 square col-4@mobile square@mobile"
					style="background-image: url('<?= $photo['thumb'] ?>')">
						<span><span><?= $photo['title'] ?></span></span>
					</a>

				<?php endforeach ?>
				</div>

<?php if (!$ajax): ?>

			</section>

			<?php if ($has_more): ?>
			<section class="gallery page--section" eg-grid="col-12 align-center">
				<p>
					<a href="#" class="call-to-action gallery--load-more">
						Load more photos...
					</a>
				</p>
			</section>
			<?php endif; ?>

		</div>

	</div></div>
</div>

<?php endif; ?>

<?php endwhile; ?>

<?php if (!$ajax): ?>

<?php get_template_part('parts/gallery-video-slider'); ?>

<?php get_template_part('parts/site-footer'); ?>
<?php get_template_part('parts/html-footer'); ?>

<?php endif; ?>
