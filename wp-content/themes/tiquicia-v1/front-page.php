<?php get_template_part('parts/html-header'); ?>

<?php get_template_part('parts/site-nav'); ?>

<?php define('MAIN_SLIDER', true); get_template_part('parts/main-slider'); ?>

<?php get_template_part('parts/about-us-and-newest-tours') ?>
<?php get_template_part('parts/featured-tour') ?>
<?php get_template_part('parts/testimonials') ?>
<?php get_template_part('parts/tour-types-grid') ?>
<?php get_template_part('parts/find-us') ?>

<?php get_template_part('parts/site-footer'); ?>
<?php get_template_part('parts/html-footer'); ?>