<?php

	$slides = [];

	$thumbnail_id = get_post_thumbnail_id();
	$image = wp_get_attachment_image_src($thumbnail_id, 'tiquicia-hero-image');

	if (!empty($image)) {

		$slides[] = [
			'image' => $image[0]
		];

	}

	$args = [
		'posts_per_page' => -1,
		'post_type' => 'slide',
		'orderby' => 'meta_value_num',
		'meta_key' => 'position',
		'order' => 'ASC',
		'meta_query' => [
			[
				'key' => 'active',
				'value' => '1'
			],
			[
				'key' => 'bound-to',
				'value' => defined('CUSTOM_SLIDESHOW_BINDING') ?
					CUSTOM_SLIDESHOW_BINDING : '@home'
			]
		]
	];

	$slidesObj = new WP_Query($args);

	if ($slidesObj->have_posts()) {

		while ($slidesObj->have_posts()) {

			$slidesObj->the_post();

			$thumbnail_id = get_post_thumbnail_id($post_id);
			$image = wp_get_attachment_image_src($thumbnail_id, 'tiquicia-hero-image');

			if (empty($image)) {
				continue;
			}

			$slides[] = [
				'image' => $image[0]
			];

		}

		wp_reset_postdata();

	}

	if (empty($slides)) {
		return;
	}

?>

<div class="main-slider" eg-grid="loose-container">

	<div eg-grid="row">

		<div class="slider slider--main" eg-grid="col-12">

			<?php tt_inject_slider_controls('main-slider', count($slides)) ?>

			<div class="slider--slides-container" id="main-slider-slider--slides-container">

				<?php $i = 0; foreach ($slides as $slide): $i++; ?>
				<div class="slider--slide" id="main-slider--slide-<?= $i ?>"
					style="background-image: url('<?= $slide['image'] ?>')"></div>
				<?php endforeach; ?>

			</div>

			<div class="slider--overlay">
				<div class="slider--content">

					<?php if (defined('MAIN_SLIDER')): ?>

					<h1>
						<strong>Welcome to</strong>
						Tiquicia Tours Puntarenas
					</h1>

					<p>
						Explore what we can offer to you.
					</p>

					<?php else: ?>

					<h1 class="on-tour">
						Tiquicia Tours Puntarenas
					</h1>

					<p class="on-tour">
						<?= get_the_title(); ?>
					</p>

					<?php endif; ?>

				</div>
			</div>

		</div>

	</div>

</div>