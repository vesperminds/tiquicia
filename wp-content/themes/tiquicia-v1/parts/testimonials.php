<?php
$testimonials = get_all_reviews();
?>
<?php if ($testimonials): ?>
<div class="testimonials page--section" eg-grid="container">

	<div eg-grid="row">

		<section class="copy copy--centered" eg-grid="col-12">

			<h2><u>Testimonials</u></h2>
			<h3 class="sub-title"><u>Reviews from our TripAdvisor Community</u></h3>

			<div class="slider slider--testimonials">

				<?php tt_inject_slider_controls('testimonials-slider', count($testimonials)) ?>

				<div class="slider--slides-container" id="testimonials-slider-slider--slides-container">

					<?php $i = 0; foreach ($testimonials as $testimonial): $i++; ?>
					<div class="slider--slide" id="testimonials-slider--slide-<?= $i ?>">

						<p class="quote">
							"<?= $testimonial['content'] ?>"
						</p>
						<p class="quote-meta">
							<strong><?= $testimonial['author'] ?></strong><br>
							<em><?= $testimonial['date'] ?></em>
						</p>

					</div>
					<?php endforeach; ?>

				</div>

			</div>

		</section>

	</div>

</div>
<?php endif; ?>
