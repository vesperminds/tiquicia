<div class="tour-types-grid page--section" eg-grid="container">

	<div eg-grid="row">

		<section class="tour-types tour-types--copy" eg-grid="col-4 col-8@tablet col-12@mobile">
			<span><span>
				We know the difference between a good tour and great experience,
				just let us know what are you looking for.
			</span></span>
			<u></u>
		</section>

		<a class="tour-types tour-types--link tour-types--city-tour"
		href="<?= vp_url('/tours/city') ?>" eg-grid="col-2 col-4@tablet col-6@mobile">
			<span><span>
				<i></i>
				City<br>
				Tour
			</span></span>
			<u></u>
		</a>

		<a class="tour-types tour-types--link tour-types--island-tour"
		href="<?= vp_url('/tours/island') ?>" eg-grid="col-2 col-4@tablet col-6@mobile">
			<span><span>
				<i></i>
				Island<br>
				Tour
			</span></span>
			<u></u>
		</a>

		<a class="tour-types tour-types--link tour-types--skywalk-tour"
		href="<?= vp_url('/tours/skywalk') ?>" eg-grid="col-2 col-4@tablet col-6@mobile">
			<span><span>
				<i></i>
				Skywalk<br>
				Tour
			</span></span>
			<u></u>
		</a>

		<a class="tour-types tour-types--link tour-types--rain-forest-tour"
		href="<?= vp_url('/tours/rain-forest') ?>" eg-grid="col-2 col-4@tablet col-6@mobile">
			<span><span>
				<i></i>
				Rain Forest<br>
				Tour
			</span></span>
			<u></u>
		</a>

	</div>

</div>