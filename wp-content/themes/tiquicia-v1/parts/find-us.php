<div class="find-us page--section" eg-grid="container">

	<div eg-grid="row">

		<section class="copy copy--centered" eg-grid="col-8 push-2 col-10@tablet push-1@tablet col-12@mobile push-0@mobile">

			<h2><u>Find Us</u></h2>

			<p>
				We are waiting for your visit is a pleasure to serve. We can be
				found at the hotel beach resort double tree by Hilton, Puntarenas
				that way you will be closer to provide our excellent service.
			</p>

			<p>
				Also, we will be waiting on the dock of Puntarenas to make the most
				of the time and continue our journey of adventure, fun and
				relaxation.
			</p>

			<p class="separated">
				<a class="call-to-action" target="_blank" href="https://www.google.com/maps/@9.9686796,-84.7395791,17.64z">Google Maps</a>
				&nbsp;
				<a class="call-to-action" href="<?= vp_url('/contact-us') ?>">Contact Us</a>
			</p>

		</section>

	</div>

</div>