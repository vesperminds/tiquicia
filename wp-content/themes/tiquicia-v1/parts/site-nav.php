<?php

	$nav = [

		[
			'label' => 'Home',
			'href' => vp_url('/'),
			'check' => function() {
				return $_SERVER['REQUEST_URI'] == '/';
			}
		],
		[
			'label' => 'Tours',
			'href' => vp_url('/tours'),
			'check' => function() {
				return strpos($_SERVER['REQUEST_URI'], '/tour') !== false;
			}
		],
		[
			'label' => 'Gallery',
			'href' => vp_url('/gallery'),
			'check' => function() {
				return strpos($_SERVER['REQUEST_URI'], '/gallery') !== false;
			}
		],
		[
			'label' => '<i></i>',
			'class' => 'nav--logo',
			'no-mobile' => true
		],
		[
			'label' => 'About Us',
			'href' => vp_url('/about-us'),
			'check' => function() {
				return strpos($_SERVER['REQUEST_URI'], '/about-us') !== false;
			}
		],
		[
			'label' => 'Contact Us',
			'href' => vp_url('/contact-us'),
			'check' => function() {
				return strpos($_SERVER['REQUEST_URI'], '/contact-us') !== false;
			}
		],
		[
			'label' => '<i></i>',
			'class' => 'nav--facebook',
			'href' => 'http://www.facebook.com/tiquiciatourspuntarenas',
			'no-mobile' => true,
			'external' => true
		],
		[
			'label' => 'Facebook',
			'href' => 'http://www.facebook.com/tiquiciatourspuntarenas',
			'no-desktop' => true,
			'external' => true
		],
		[
			'label' => '<i></i>',
			'class' => 'nav--twitter',
			'href' => 'http://twitter.com/tiquiciatours',
			'no-mobile' => true,
			'external' => true
		],
		[
			'label' => 'Twitter',
			'href' => 'http://twitter.com/tiquiciatours',
			'no-desktop' => true,
			'external' => true
		],


	];

	$active_index = -1;

	for ($i = 0; $i < count($nav); $i++) {

		if (!empty($nav[$i]['check']) && is_callable($nav[$i]['check'])) {

			if (call_user_func($nav[$i]['check'])) {
				$active_index = $i;
				break;
			}

		}

	}

?>

<div class="nav--hamburger"></div>

<nav class="nav nav--mobile">

	<div class="logo"></div>

	<ul>

		<?php $i = -1; foreach ($nav as $item): $i++; ?>
		<?php
			if (!empty($item['no-mobile'])) {
				continue;
			}
		?>
		<li class="<?= $active_index === $i ? 'active' : '' ?>  <?= !empty($item['class']) ? $item['class'] : '' ?>">
			<?php if (!empty($item['href'])): ?>
			<a <?= !empty($item['external']) ? 'target="_blank"' : '' ?> href="<?= $item['href'] ?>"><?= $item['label'] ?></a>
			<?php else: ?>
			<?= $item['label'] ?>
			<?php endif; ?>
		</li>
		<?php endforeach; ?>

	</ul>

</nav>

<nav class="nav nav--top">

	<div eg-grid="container">

		<div eg-grid="row">

			<ul class="nav--root" eg-grid="col-12">

				<?php $i = -1; foreach ($nav as $item): $i++; ?>
				<?php
					if (!empty($item['no-desktop'])) {
						continue;
					}
				?>
				<li class="<?= $active_index === $i ? 'active' : '' ?>  <?= !empty($item['class']) ? $item['class'] : '' ?>">
					<?php if (!empty($item['href'])): ?>
					<a <?= !empty($item['external']) ? 'target="_blank"' : '' ?> href="<?= $item['href'] ?>"><?= $item['label'] ?></a>
					<?php else: ?>
					<?= $item['label'] ?>
					<?php endif; ?>
				</li>
				<?php endforeach; ?>

			</ul>

		</div>

	</div>

</nav>
