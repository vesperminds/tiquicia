<?php

	$f_id = get_option('tiquicia_home_featured_tour');

	if (!empty($f_id)) {

		$featured = tt_get_all_tours([
			'include' => (int) $f_id,
			'limit' => 1,
			'thumbsize' => 'tiquicia-hero-image'
		]);

	}

?>
<?php if (!empty($featured)): ?>
<div class="featured-tour page--section" eg-grid="loose-container">

	<div eg-grid="row">

		<section class="hero" eg-grid="col-12" style="background-image: url('<?= $featured[0]['thumb'] ?>')">

			<div class="hero--content hero--content-centered">

				<h2>Featured Tour</h2>
				<p>
					<?= $featured[0]['title'] ?>
				</p>
				<p class="has-call-to-action">
					<a class="call-to-action call-to-action--inverted" href="<?= $featured[0]['permalink'] ?>">View Tour</a>
				</p>

			</div>

		</section>

	</div>

</div>
<?php endif; ?>