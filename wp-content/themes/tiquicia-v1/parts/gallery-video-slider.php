<?php
	$videos = [];
	$ids = get_option('tiquicia_gallery_videos', '');

	if ($ids) {
		$videos = array_map(function($item) {
			return trim($item);
		}, explode(',', $ids));
	}
?>
<?php if ($videos): ?>
<div class="video-slider page--section" eg-grid="container">

	<div eg-grid="row">

		<section class="copy copy--centered" eg-grid="col-12">

			<h2>
				<u>
					Videos from Our Tours
					<a class="link--follow-us" href="https://www.youtube.com/channel/UCMDdXi8zcQ56Tkma1dqO5Dg?sub_confirmation=1" target="_blank">
						Follow Us
					</a>
				</u>
			</h2>
			<h3 class="sub-title">
				<u>
					Check out some videos from our recent tours
					and discover what we can offer you.
				</u>
			</h3>

			<div class="slider slider--no-auto slider--videos">

				<?php tt_inject_slider_controls('video-slider', count($videos)) ?>

				<div class="slider--slides-container" id="video-slider-slider--slides-container">

					<?php $i = -1; foreach($videos as $video): $i++; ?>
					<div class="slider--slide" id="video-slider--slide-<?= $i + 1 ?>">

						<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $video ?>"
							frameborder="0" allowfullscreen></iframe>

					</div>
					<?php endforeach; ?>

				</div>

			</div>

		</section>

	</div>

</div>
<?php endif; ?>
