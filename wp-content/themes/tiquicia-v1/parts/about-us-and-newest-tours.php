<?php

	$tour_a_id = get_option('tiquicia_home_tour_a');
	$tour_b_id = get_option('tiquicia_home_tour_b');
	$tour_c_id = get_option('tiquicia_home_tour_c');

	if (!empty($tour_a_id)) {

		$tours = tt_get_all_tours([
			'include' => [
				(int) $tour_a_id,
				(int) $tour_b_id,
				(int) $tour_c_id
			],
			'limit' => 3,
			'orderby' => 'post__in'
		]);

	}

?>
<div class="about-us-and-newest-tours page--section" eg-grid="container">

	<div eg-grid="row align-middle">

		<section class="copy copy--light about-us--copy" eg-grid="col-8 col-12@mobile">

				<h2><u>About Us</u></h2>

				<p>
					Tiquicia Tours is a family company with over 10 years experience in the
					tourism industry. This organization has been renovated in order to have
					a better service to our customers.
				</p>

				<p>
					Our goal is that you know the wonders of Costa Rica, we take care to
					provide specialist care service for you to relax and enjoy our beaches,
					restaurants, hotels, Volcanos and an endless number of adventures such
					as: rafting, Canopy, Jungle Crocodile Safari, Snorkeling and many more
					in which our experienced guides will get ready for fun.
				</p>

				<p class="separated">
					<a class="call-to-action call-to-action--light" href="<?= vp_url('/about-us') ?>">
						Read more &hellip;
					</a>
				</p>

		</section>

		<section class="blocks newest-tours" eg-grid="col-4 col-12@mobile">

			<div eg-grid="row">

				<?php if (!empty($tours[0])): ?>
				<a class="blocks--element newest-tours--a" href="<?= $tours[0]['permalink'] ?>"
					eg-grid="col-6 square" style="background-image: url('<?= $tours[0]['thumb'] ?>')">
					<span><span>
						<?= $tours[0]['title'] ?>
					</span></span>
				</a>
				<?php endif; ?>

				<?php if (!empty($tours[1])): ?>
				<a class="blocks--element newest-tours--b" href="<?= $tours[1]['permalink'] ?>"
					eg-grid="col-6 square" style="background-image: url('<?= $tours[1]['thumb'] ?>')">
					<span><span>
						<?= $tours[1]['title'] ?>
					</span></span>
				</a>
				<?php endif; ?>

				<a class="blocks--element newest-tours--tripadvisor" target="_blank" href="http://www.tripadvisor.com/Attraction_Review-g309287-d1754802-Reviews-Tiquicia_Tours-Puntarenas_Province_of_Puntarenas.html" eg-grid="col-6 square">
					<span>
						<span class="block-tour--tripadvisor"></span>
					</span>
				</a>

				<?php if (!empty($tours[2])): ?>
				<a class="blocks--element newest-tours--c" href="<?= $tours[2]['permalink'] ?>"
					eg-grid="col-6 square" style="background-image: url('<?= $tours[2]['thumb'] ?>')">
					<span><span>
						<?= $tours[2]['title'] ?>
					</span></span>
				</a>
				<?php endif; ?>

			</div>

		</section>

	</div>

</div>