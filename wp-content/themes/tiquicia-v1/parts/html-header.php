<?php defined('ABSPATH') or die; ?>
<!DOCTYPE html>
<html lang="en" class="TiquiciaTours">
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?= tt_title() ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="msapplication-tap-highlight" content="no">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?= vpth_path('/dist/css/main.css') ?>">

	<?php wp_head(); ?>
	
	<link rel="icon" href="<?= vpth_path('/favicon.ico') ?>" type="image/x-icon"/>
	<link rel="shortcut icon" href="<?= vpth_path('/favicon.ico') ?>" type="image/x-icon"/>

</head>
<body <?= body_class() ?> ng-controller="appCtrl">
