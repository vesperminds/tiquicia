<?php

	$args = [];

	if (isset($_FILTER_TOURS)) {
		$args['taxonomyslug'] = wp_slash($_FILTER_TOURS);
	}

	$tours = tt_get_all_tours($args);

?>
<?php get_template_part('parts/html-header'); ?>
<?php get_template_part('parts/site-nav'); ?>

<?php define('MAIN_SLIDER', true); get_template_part('parts/main-slider'); ?>

<div class="page-tours page--section" eg-grid="container">

	<div eg-grid="row">

		<?php if (!empty($tours)): ?>

			<section class="copy copy--centered" eg-grid="col-8 push-2 col-10@tablet push-1@tablet col-12@mobile push-0@mobile">

				<h2><u>Our Tours</u></h2>

				<p>
					Costa Rica is a wonderful destination for active travelers offering a wide range of opportunities for specific sports and adventure in a small area. Whatever your taste in recreation, Costa Rica has something for you.
				</p>

				<p>
					National Parks of Costa Rica are a smorgasbord of outdoor activities both for those who like adrenaline and who prefer the quieter admire nature.
				</p>

			</section>

			<section class="blocks" eg-grid="col-12">
				<div eg-grid="row">
				<?php foreach ($tours as $tour): ?>

				<a class="blocks--element" href="<?= $tour['permalink'] ?>"
					eg-grid="col-3 col-6@mobile square square@mobile"
					style="background-image: url('<?= $tour['thumb'] ?>')">
					<span><span>
						<?= $tour['title'] ?>
					</span></span>
				</a>

				<?php endforeach; ?>
				</div>
			</section>

		<?php else: ?>

			<div eg-grid="col-12" class="copy copy--light copy--centered">
				<p>
					Oops! There are no tours matching your criteria.
				</p>
			</div>

		<?php endif; ?>

	</div>

</div>

<?php get_template_part('parts/site-footer'); ?>
<?php get_template_part('parts/html-footer'); ?>