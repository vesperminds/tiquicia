<?php

$selector_unit = 'rem';
$selector_size = 1.8;
$selector_margin = 1.4;

$selector_step = $selector_size + $selector_margin;
$selector_total = ($slides * $selector_step) - $selector_margin;
$selector_initial = 0 - ($selector_total / 2);

?>

<style type="text/css">

#<?= $id ?>-slider--slides-container {
	width: <?= ($slides + 1) * 100 ?>%;
}

#<?= $id ?>-slider--slides-container .slider--slide {
	width: <?= round((1 / ($slides + 1)) * 100, 1) ?>%;
}

<?php $i = -1; for ($slide = 1; $slide <= $slides; $slide++): $i++; ?>

#<?= $id ?>--slide-selector--<?= $slide ?> {
	margin-left: <?= $selector_initial + ($selector_step * $i) ?><?= $selector_unit ?>;
}

#<?= $id ?>--slide-control--<?= $slide ?>:checked ~ #<?= $id ?>-slider--slides-container {
	margin-left: <?= -($i * 100) ?>%;
}

<?php endfor; ?>

</style>

<?php $checked = false; for ($slide = 1; $slide <= $slides; $slide++): ?>
<input <?php if (!$checked) { echo 'checked="checked"'; $checked = true; } ?> type="radio"
	name="<?= $id ?>-slide" class="slider--slide-control" id="<?= $id ?>--slide-control--<?= $slide ?>" />
<label for="<?= $id ?>--slide-control--<?= $slide ?>"
	class="slider--slide-selector"
	id="<?= $id ?>--slide-selector--<?= $slide ?>"
	<?= $slides <= 1 ? 'style="display: none;"' : '' ?> ></label>
<?php endfor; ?>