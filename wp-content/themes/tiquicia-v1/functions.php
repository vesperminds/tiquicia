<?php defined('ABSPATH') or die;

if (!defined('VESPERPRESS')) {
	throw new \Exception('VesperPress is not loaded');
}

define('DEBUG_MODE', strpos($_SERVER['SERVER_NAME'], '.local.host') !== false);

/* Scripts and stylesheet */

add_action('wp_enqueue_scripts', function() {

	wp_deregister_script('jquery');
	wp_register_script(
		'jquery',
		vpth_path('/js/jquery-2.1.4.min.js'),
		null,
		vpth_version('/js/jquery-2.1.4.min.js'),
		true
	);

	wp_deregister_script('main');
	wp_register_script(
		'main',
		vpth_path('/js/main.js'),
		null,
		vpth_version('/js/main.js'),
		true
	);



	wp_enqueue_script('jquery');
	wp_enqueue_script('main');

});

/* Content fix */

add_filter( 'the_content', function ($content) {

	// clean up p tags around block elements
	$content = preg_replace( [
		'#<p>\s*<(div|aside|section|article|header|footer)#',
		'#</(div|aside|section|article|header|footer)>\s*</p>#',
		'#</(div|aside|section|article|header|footer)>\s*<br ?/?>#',
		'#<(div|aside|section|article|header|footer)(.*?)>\s*</p>#',
		'#<p>\s*</(div|aside|section|article|header|footer)#',
	], [
		'<$1',
		'</$1>',
		'</$1>',
		'<$1$2>',
		'</$1',
	], $content);

	return preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)*(\s|&nbsp;)*</p>#i', '', $content);

}, 20, 1 );

/* Images */

add_theme_support('post-thumbnails', [
	'post',
	'page',
	'tour',
	'photo',
	'slide',
]);

add_action('init', function() {

	add_image_size('tiquicia-hero-image', 1920, 720);
	add_image_size('tiquicia-mobile-hero-image', 960, 360);
	add_image_size('tiquicia-post-thumbnail', 600, 600, true);
	add_image_size('tiquicia-post-thumbnail-cover', 600, 600);
	add_image_size('tiquicia-gallery-image', 1200, 1200);

});

/* Slider */

function tt_inject_slider_controls($id, $slides = 1) {
	include __DIR__ . '/elements/slider-controls.php';
}

/* Title */

$_tiquicia_overrideTitle = null;

function tt_title_suffix() {
	return ' &mdash; Tiquicia Tours Puntarenas';
}

function tt_set_page_title($title = 'Tiquicia Tours Puntarenas') {

	global $_tiquicia_overrideTitle;

	if (empty($title)) {
		return;
	}

	$_tiquicia_overrideTitle = $title;

}

function tt_title() {
	return wp_title(tt_title_suffix(), true, 'right');
}

add_filter('wp_title', function($title) {
	global $_tiquicia_overrideTitle;

	if (!empty($_tiquicia_overrideTitle)) {
		return $_tiquicia_overrideTitle . tt_title_suffix();
	}

	if (empty($title) && (is_home() || is_front_page())) {
	  return 'Tiquicia Tours Puntarenas';
	}

	return $title;
});

/* JSON */

function tt_json_output($data) {

	@header('Content-Type: application/json; charset=utf-8');
	echo json_encode($data);

}

/* Form (Contact and Tour Info) */

define('EMAIL_EOL', "\r\n");

$tt_form_handlers = [

	'contact' => function($step, $data) {

		if ($step == 'validate') {

			$errors = [];

			if (empty($data['fullname'])) {
				$errors[] = 'fullname';
			}

			if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
				$errors[] = 'email';
			}

			if (empty($data['message'])) {
				$errors[] = 'message';
			}

			return $errors;

		}
		elseif ($step == 'compose') {

			$message = [];

			$message[] = 'Contact Request';
			$message[] = '---------------';
			$message[] = '';
			$message[] = 'Full Name: ' . $data['fullname'];
			$message[] = 'Email: ' . $data['email'];
			$message[] = 'Message:';
			$message[] = $data['message'];

			return $message;

		}
		elseif ($step == 'subject') {
			return 'Contact Form';
		}
		elseif ($step == 'compose-user') {

			$message = [];

			$message[] = 'Hey ' . $data['fullname'] . ',';
			$message[] = '';
			$message[] = 'Thank you for getting in touch with us. We\'ll reply you as soon as possible.';
			$message[] = '';
			$message[] = 'Thanks,';
			$message[] = '- The Tiquicia Tours team';


			return $message;

		}
		elseif ($step == 'subject-user') {
			return 'Thank you for contacting us';
		}

		return false;

	},
	'rinfo' => function($step, $data) {

		if ($step == 'validate') {

			$errors = [];

			if (empty($data['fullname'])) {
				$errors[] = 'fullname';
			}

			if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
				$errors[] = 'email';
			}

			if (empty($data['date'])) {
				$errors[] = 'date';
			}

			if (empty($data['address'])) {
				$errors[] = 'address';
			}

			if (!isset($data['adults']) || !is_numeric($data['adults'])) {
				$errors[] = 'adults';
			}

			if (!isset($data['kids']) || !is_numeric($data['kids'])) {
				$errors[] = 'kids';
			}

			return $errors;

		}
		elseif ($step == 'compose') {

			$message = [];

			$message[] = 'Info Request';
			$message[] = '---------------';
			$message[] = '';
			$message[] = 'Tour: ' . $data['tourname'];
			$message[] = 'Full Name: ' . $data['fullname'];
			$message[] = 'Email: ' . $data['email'];
			$message[] = 'Departure date: ' . $data['date'];
			$message[] = 'Pickup Address: ' . $data['address'];
			$message[] = '# Adults: ' . $data['adults'];
			$message[] = '# Kids: ' . $data['kids'];
			$message[] = 'Special request:';
			$message[] = $data['message'];

			return $message;

		}
		elseif ($step == 'subject') {
			return 'Request Info: ' . $data['tourname'];
		}
		elseif ($step == 'compose-user') {

			$message = [];

			$message[] = 'Hey ' . $data['fullname'] . ',';
			$message[] = '';
			$message[] = 'Thank you for getting in touch with us requesting information about ' . $data['tourname'] . '. We\'ll get back to you as soon as possible.';
			$message[] = '';
			$message[] = 'Thanks,';
			$message[] = '- The Tiquicia Tours team';


			return $message;

		}
		elseif ($step == 'subject-user') {
			return 'Thank you for contacting us regarding ' . $data['tourname'];
		}

		return false;

	}

];

function tt_handle_form() {

	global $tt_form_handlers;

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		return;
	}

	if (empty($_POST['_fh']) || empty($tt_form_handlers[$_POST['_fh']])) {
		return;
	}

	$fh = $_POST['_fh'];

	$data = $_POST;

	foreach ($data as $key => $value) {
		$data[$key] = trim(strip_tags($value));
	}

	$validationErrors = $tt_form_handlers[$fh]('validate', $data);

	if (!empty($validationErrors)) {
		tt_json_output([
			'done' => false,
			'error' => 'E_INVALID_FIELDS',
			'message' => 'One or more fields are not valid',
			'details' => $validationErrors
		]);
		exit;
	}

	$admin_email = get_option('admin_email');
	$admin_rcpt = [ $admin_email ];

	if (!DEBUG_MODE) {
		$admin_rcpt[] = 'tiquiciatourspuntarenas@gmail.com';
	}

	$subject = $tt_form_handlers[$fh]('subject', $data);
	$message = $tt_form_handlers[$fh]('compose', $data);

	if (!DEBUG_MODE) {
		wp_mail($admin_rcpt, $subject, implode(EMAIL_EOL, $message));
	}

	$subjectUser = $tt_form_handlers[$fh]('subject-user', $data);
	$messageUser = $tt_form_handlers[$fh]('compose-user', $data);

	if (!DEBUG_MODE) {
		wp_mail($data['email'], $subjectUser, implode(EMAIL_EOL, $messageUser));
	}

	tt_json_output([
		'done' => true
	]);
	exit;

}