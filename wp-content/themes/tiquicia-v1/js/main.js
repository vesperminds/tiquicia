/* Mobile Menu */

$(function() {

	var nav = $('.nav--mobile');

	if (!nav.length) {
		return;
	}

	var button = $('.nav--hamburger'),
		body = $('body:first'),
		stateClass = 'state--mobile-menu-open',
		logo = nav.find('.logo');

	var closeMenu = function() {

		if (!body.hasClass(stateClass)) {
			return;
		}

		body.removeClass(stateClass);

	};

	nav.on('click', function(ev) {
		ev.stopPropagation();
	});

	nav.find('a[href]').on('click', function() {
		window.setTimeout(closeMenu, 250);
	});

	body.on('click', closeMenu);
	logo.on('click', closeMenu);

	button.on('click', function(ev) {

		ev.stopPropagation();
		body.toggleClass(stateClass);

	});

});

/* Lighboxes */

$(function() {

	var body = $('body:first'),
		overlay = $('<div></div>'),
		ctaWrapper = $('<div></div>'),
		closeBtn = $('<a></a>');

	overlay
		.addClass('edgebal--overlay')
		.appendTo(body);

	ctaWrapper
		.addClass('edgebal--overlay-cta-wrapper')
		.appendTo(overlay);

	closeBtn
		.addClass('edgebal--overlay-cta call-to-action call-to-action--inverted call-to-action--wide')
		.attr('href', '#')
		.text('Close')
		.appendTo(ctaWrapper);

	var closeOverlay = function() {

		if (!overlay.hasClass('edgebal--overlay-active')) {
			return;
		}

		body.removeClass('edgebal--has-overlay-active');
		overlay.removeClass('edgebal--overlay-active');

	};

	overlay.on('click', function(ev) {
		ev.stopPropagation();
		ev.preventDefault();
		closeOverlay();
	});

	body.on('click', function(ev) {
		closeOverlay();
	});

	body.on('click', 'a[href][eg-lightbox]', function(ev) {

		ev.stopPropagation();
		ev.preventDefault();

		var href = $(this).attr('href');

		body.addClass('edgebal--has-overlay-active');
		overlay.addClass('edgebal--overlay-active');
		overlay.css('background-image', 'url(\'' + href + '\')');

	});

});

/* Form */

$(function() {

	var form = $('.form--ajax');

	if (!form.length) {
		return;
	}

	var
		ctaBtn = $('.form--cta'),
		sendBtn = form.find('[type=submit]'),
		success = $('.form--ajax-success');

	if (ctaBtn && ctaBtn.length) {
		form.hide();
	}

	var prevalidate = function(form) {

		var errors = [];

		form.find('[required]').each(function() {

			var el = $(this),
				name = el.attr('name');

			if (!name) {
				return;
			}

			if (!el.val().length) {
				if (!el.data('__set')) {
					errors.push(name);
					el.data('__set', true);
				}
			}

		});

		form.find('[type="email"]').each(function() {

			var el = $(this),
				name = el.attr('name');

			if (!name) {
				return;
			}

			if (!el.val()) {
				return;
			}

			if (el.val().indexOf('@') < 0 || el.val().indexOf('.') < 0) {
				if (!el.data('__set')) {
					errors.push(name);
					el.data('__set', true);
				}
			}

		});

		return errors;

	};

	var setErrors = function(form, errors) {

		var focus = null;

		$.each(errors, function(idx, field) {

			var el = form.find('[name="' + field + '"]'),
				wrapper = el.closest('.form--field');

			if (!el || !el.length) {
				return;
			}

			el.addClass('form--error');
			wrapper.addClass('form--error-wrapper');

			el.on('focus.vpErr click.vpErr blur.vpErr', function() {

				el.removeClass('form--error');
				wrapper.removeClass('form--error-wrapper');

				window.setTimeout(function() {
					el.off('.vpErr');
				}, 0);

			});

			if (!focus) {
				focus = el;
			}

		});

		if (focus) {
			focus.focus();
		}

	};

	ctaBtn.on('click', function(ev) {

		ev.preventDefault();
		ctaBtn.hide();
		form.slideDown();

	});

	form.find('[type="submit"]').on('click', function(ev) {

		ev.preventDefault();

		var action = form.attr('action');

		if (!action) {
			action = window.location.href;
		}

		var errors = prevalidate(form);

		if (errors.length) {
			setErrors(form, errors);
			return;
		}

		sendBtn.hide();

		$.ajax({
			url: action,
			type: 'post',
			dataType: 'json',
			data: form.serialize()
		}).done(function(data) {

			if (data.done) {

				form.hide();
				success.fadeIn();
				return;

			}

			if (typeof data.error !== 'undefined') {

				if (data.error === 'E_INVALID_FIELDS') {
					setErrors(form, data.details);
					return;
				}

			}

		}).fail(function() {

			alert('There was an error processing your request. Please try again later.');

		}).always(function() {

			sendBtn.fadeIn();

		});

	});

});

/* Slider */

$(function() {

	var sliders = $('.slider');

	if (!sliders.length) {
		return;
	}

	var nextSlide = function(slider) {

		var sel = '.slider--slide-control',
			current = slider.find(sel + ':checked'),
			next;

		if (current.length) {
			next = current.nextAll(sel + ':first');
		}

		if (!next || !next.length) {
			next = slider.find(sel + ':first');
		}

		next.prop('checked', true);

	};

	var stopTimer = function(slider) {

		var timer = slider.data('_slider_timer');

		if (!timer) {
			return;
		}

		window.clearInterval(timer);
		slider.data('_slider_timer', false);

	};

	var startTimer = function(slider) {

		var msec = 6 * 1000;

		stopTimer(slider);
		slider.data('_slider_timer', window.setInterval(function() {
			nextSlide(slider);
		}, msec));

	};

	sliders.each(function() {
		$this = $(this);
		
		if ($this.hasClass('slider--no-auto')) return;
		
		startTimer($this);
	});

});


/* Gallery */

$(function() {

	var btn = $('.gallery--load-more'),
		wrapper = $('.gallery--ajax-wrapper');

	if (!btn.length) {
		return;
	}

	var page = 1,
		href = window.location.href;

	btn.on('click', function(ev) {

		ev.preventDefault();

		page += 1;

		var url = href;

		if (url.indexOf('#') > -1) {
			url = url.substr(0, url.indexOf('#'));
		}

		url += url.indexOf('?') > -1 ? '&' : '?';
		url += 'ajax=1&_page=' + page;

		btn.hide();

		$.ajax({
			url: url,
			type: 'get',
			dataType: 'html'
		}).done(function(html) {

			var $html = $($.trim(html)),
				hasMore = parseInt($html.attr('data-has-more'), 10);

			wrapper.append($html);

			if (hasMore) {
				btn.fadeIn();
			}

		});

	});

});
