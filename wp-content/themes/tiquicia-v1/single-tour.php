<?php
	tt_handle_form();
?>

<?php get_template_part('parts/html-header'); ?>
<?php get_template_part('parts/site-nav'); ?>

<?php while (have_posts()): the_post(); ?>

<?php define('CUSTOM_SLIDESHOW_BINDING', 'tour:' . get_the_ID()); get_template_part('parts/main-slider'); ?>

<?php

$meta = vp_fetch_meta();

$videos = [];

if ($meta->get('youtube-ids')) {
	$videos = array_map(function($item) {
		return trim($item);
	}, explode(',', $meta->get('youtube-ids')));
}

?>

<div class="page-single page-single-tour page--section" eg-grid="container">
	<div eg-grid="row"><div eg-grid="col-10 wrap-1 col-12@tablet wrap-0@tablet col-12@mobile wrap-0@mobile">

		<div class="title-and-meta" eg-grid="row align-middle">

				<div eg-grid="col-8 col-12@mobile" class="copy copy--light">

				<?php if ($meta->any(['duration', 'what-to-bring', 'highlights'])): ?>

					<div class="meta-wrapper">

						<div class="meta">
							<span class="label">Duration:</span> <?= $meta->get('duration') ?>
						</div>

						<div class="meta">
							<span class="label">What to bring:</span> <?= $meta->get('what-to-bring') ?>
						</div>

						<div class="meta">
							<span class="label">Highlights:</span> <?= $meta->get('highlights') ?>
						</div>

					</div>

				<?php endif; ?>

			</div>

			<div eg-grid="col-4 hide@mobile align-right">

				<p class="separated">
					<a class="call-to-action call-to-action--light" href="<?= vp_url('/tours') ?>">Back to our tours</a>
				</p>

			</div>

		</div>

		<div eg-grid="row">

			<section class="copy copy--light copy--centered" eg-grid="col-12">

				<?php the_content() ?>

				<p class="separated">
					<a class="call-to-action call-to-action--wide call-to-action--important form--cta" href="#">Request info</a>
				</p>

			</section>

			<section class="copy copy--centered" eg-grid="col-12">
				<div class="form--ajax-success" style="display: none;">

					<p class="big">We've received your information</p>

					<p>
						Thank you for request info about this tour with us.
						You will receive a confirmation email shortly.
						Please also check your JUNK box.
					</p>

					<p>
						We’re here to help you, in case that you experience
						any issues, feel free to call (506) 8814 - 8609.
					</p>

				</div>
			</section>

			<form class="form form--ajax" eg-grid="col-12"
			action="<?= get_the_permalink() ?>" method="post" novalidate>

				<input type="hidden" name="_fh" value="rinfo">
				<input type="hidden" name="tourname" value="<?= htmlspecialchars(html_entity_decode(get_the_title()), ENT_COMPAT) ?>">

				<div class="form--field" eg-grid="row align-middle">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="fullname">Full Name</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper">
							<input type="text" name="fullname" id="fullname" required />
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-middle">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="email">Email</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper">
							<input type="email" name="email" id="email" required />
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-middle">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="date">Departure date</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper">
							<input type="text" name="date" id="date" required />
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-middle">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="address">Pickup address</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper">
							<input type="text" name="address" id="address" required />
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-middle">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="adults">No. of adult(s)</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper form--input-small">
							<input type="number" maxlength="2" min="0" max="99" name="adults" id="adults" required />
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-middle">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="kids">No. of kid(s)</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper form--input-small">
							<input type="number" maxlength="2" min="0" max="99" name="kids" id="kids" required />
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-top">
					<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
						<label for="message">
							Please enter any
							special request
							you would like
							to make
						</label>
					</div>
					<div eg-grid="col-8 col-12@mobile">
						<span class="form--input-wrapper">
							<textarea name="message" id="message"></textarea>
						</span>
					</div>
				</div>

				<div class="form--field" eg-grid="row align-middle">
					<div eg-grid="col-8 push-4 col-12@mobile push-0@mobile">
						<span class="form--note">* All the fields, except "special request", are required</span>
					</div>
				</div>

				<p class="form--submit">
					<button type="submit" class="call-to-action call-to-action--important">Request info</button>
				</p>

			</form>

			<?php if ($videos): ?>
			<section class="copy copy--centered video-slider" eg-grid="col-12">

				<h2>
					<u>
						Videos about this tour
						<a class="link--follow-us" href="https://www.youtube.com/channel/UCMDdXi8zcQ56Tkma1dqO5Dg?sub_confirmation=1" target="_blank">
							Follow Us
						</a>
					</u>
				</h2>

				<div class="slider slider--no-auto slider--videos">

					<?php tt_inject_slider_controls('video-slider', count($videos)) ?>

					<div class="slider--slides-container" id="video-slider-slider--slides-container">

						<?php $i = -1; foreach($videos as $video): $i++; ?>
						<div class="slider--slide" id="video-slider--slide-<?= $i + 1 ?>">

							<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $video ?>"
								frameborder="0" allowfullscreen></iframe>

						</div>
						<?php endforeach; ?>

					</div>

				</div>

			</section>
			<?php endif; ?>

		</div>

		<?php

			$related_tours = tt_get_all_tours([
				'limit' => 6,
				'exclude' => get_the_ID(),
				'orderby' => 'rand'
			]);

		?>

		<?php if (!empty($related_tours)): ?>
		<div class="related-tours section-tours" eg-grid="row">

			<div class="label" eg-grid="col-12">
				<p>
					You might be interested in:
				</p>
			</div>

			<section class="blocks" eg-grid="col-12">
				<div eg-grid="row">
				<?php $i = -1; foreach ($related_tours as $tour): $i++; ?>

				<a class="blocks--element" href="<?= $tour['permalink'] ?>"
					eg-grid="col-2 col-4@mobile square square@mobile <?= $i > 2 ? 'hide@mobile' : '' ?>"
					style="background-image: url('<?= $tour['thumb'] ?>')">
					<span><span>
						<?= $tour['title'] ?>
					</span></span>
				</a>

				<?php endforeach; ?>
				</div>
			</section>

		</div>
		<?php endif; ?>

		<div eg-grid="row">

			<div eg-grid="col-12 hide show@mobile">

				<p class="separated">
					<a class="call-to-action call-to-action--light" href="<?= vp_url('/tours') ?>">Back to our tours</a>
				</p>

			</div>

		</div>

	</div></div>
</div>

<?php endwhile; ?>

<?php get_template_part('parts/site-footer'); ?>
<?php get_template_part('parts/html-footer'); ?>
