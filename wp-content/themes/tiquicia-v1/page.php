<?php get_template_part('parts/html-header'); ?>
<?php get_template_part('parts/site-nav'); ?>

<?php while (have_posts()): the_post(); ?>

<?php
	$thumbnail_id = get_post_thumbnail_id();
	$image = wp_get_attachment_image_src($thumbnail_id, 'tiquicia-post-thumbnail-cover');
	$headline = get_post_meta(get_the_ID(), 'headline', true);
?>

<?php if (!empty($image)): ?>
<div class="page-single" eg-grid="loose-container">

	<div eg-grid="row">

		<section class="hero" eg-grid="col-12"
		style="background-image: url(<?= $image[0] ?>)">

			<div class="hero--content hero--content-centered">

				<h2>
					<strong>Welcome to</strong><br>
					Tiquicia Tours Puntarenas
				</h2>

				<?php if (!empty($headline)): ?>
				<p>
					<?= $headline ?>
				</p>
				<?php endif; ?>

			</div>

		</section>

	</div>

</div>
<?php endif; ?>

<div class="page-single page--section" eg-grid="container">
	<div eg-grid="row"><div eg-grid="col-10 wrap-1 col-12@tablet wrap-0@tablet col-12@mobile wrap-0@mobile">

		<div eg-grid="row">

			<section class="copy copy--light copy--centered" eg-grid="col-12">

				<h2><u><?= get_the_title(); ?></u></h2>

				<?php the_content() ?>

				<p class="separated">
					<a class="call-to-action" href="<?= vp_url('/tours') ?>">Go to our tours</a>
				</p>

			</section>

		</div>

	</div></div>
</div>

<?php endwhile; ?>

<?php get_template_part('parts/site-footer'); ?>
<?php get_template_part('parts/html-footer'); ?>