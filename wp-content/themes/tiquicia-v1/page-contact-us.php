<?php
	tt_handle_form();
?>

<?php get_template_part('parts/html-header'); ?>
<?php get_template_part('parts/site-nav'); ?>

<?php define('MAIN_SLIDER', true); get_template_part('parts/main-slider'); ?>

<div class="page-contact-us page--section" eg-grid="container">

	<div eg-grid="row">

		<section class="copy copy--light copy--centered" eg-grid="col-8 push-2 col-10@tablet push-1@tablet col-12@mobile push-0@mobile">

			<h2><u>Find Us</u></h2>

			<p>
				We are waiting for your visit is a pleasure to serve.
			</p>

			<p>
				We can be found at the hotel beach resort double tree by Hilton, Puntarenas that way you will be closer to provide our excellent service.
			</p>

			<p>
				Also we will be waiting on the dock of Puntarenas to make the most of the time and continue our journey of adventure, fun and relaxation.
			</p>

		</section>

		<section class="copy copy--centered" eg-grid="col-12">
			<div class="form--ajax-success" style="display: none;">

				<p class="big">We've received your information</p>

				<p>
					Thank you for your contact request.
					You will receive a confirmation email shortly.
					Please also check your JUNK box.
				</p>

				<p>
					We’re here to help you, in case that you experience
					any issues, feel free to call (506) 8814 - 8609.
				</p>

			</div>
		</section>

		<form class="form form--ajax" eg-grid="col-12"
		action="<?= get_the_permalink() ?>" method="post" novalidate>

			<input type="hidden" name="_fh" value="contact">

			<div class="form--field" eg-grid="row align-middle">
				<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
					<label for="fullname">Full Name</label>
				</div>
				<div eg-grid="col-8 col-12@mobile">
					<span class="form--input-wrapper">
						<input type="text" name="fullname" id="fullname" required />
					</span>
				</div>
			</div>

			<div class="form--field" eg-grid="row align-middle">
				<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
					<label for="email">Email</label>
				</div>
				<div eg-grid="col-8 col-12@mobile">
					<span class="form--input-wrapper">
						<input type="email" name="email" id="email" required />
					</span>
				</div>
			</div>

			<div class="form--field" eg-grid="row align-top">
				<div class="form--label-wrapper" eg-grid="col-4 col-12@mobile">
					<label for="message">Message</label>
				</div>
				<div eg-grid="col-8 col-12@mobile">
					<span class="form--input-wrapper">
						<textarea name="message" id="message" required></textarea>
					</span>
				</div>
			</div>

			<div class="form--field" eg-grid="row align-middle">
				<div eg-grid="col-8 push-4 col-12@mobile push-0@mobile">
					<span class="form--note">* All the fields are required</span>
				</div>
			</div>

			<p class="form--submit">
				<button type="submit" class="call-to-action call-to-action--important">Send now</button>
			</p>

			<p class="form--extra">
				Or feel free to call <a class="tel" href="tel:+50688148609">(506) 8814-8609</a>
			</p>

			<p class="form--extra">
				Excellent Service
				&bull;
				Better Prices
				&bull;
				Exclusive Tours
				&bull;
				Certified by the Costarican Tourist Board
			</p>

		</form>

	</div>


</div>

<?php get_template_part('parts/site-footer'); ?>
<?php get_template_part('parts/html-footer'); ?>