<?php defined('ABSPATH') or die;
/**
 * Plugin Name: Tiquicia Tours
 * Description: Non-theme settings for Tiquicia Tours
 * Author: Vesper Minds
 * Author URI: http://vesperminds.com/
 * Network: true
 */

require_once __DIR__ . '/tiquicia/pages.php';
require_once __DIR__ . '/tiquicia/tours.php';
require_once __DIR__ . '/tiquicia/gallery.php';
require_once __DIR__ . '/tiquicia/home-options.php';
require_once __DIR__ . '/tiquicia/slideshow.php';
require_once __DIR__ . '/tiquicia/reviews.php';

/** changing default wordpres email settings */

add_filter('wp_mail_from', function($old = '') {
	return 'no-reply@tiquiciatours.com';
});

add_filter('wp_mail_from_name', function($old = '') {
	return 'Tiquicia Tours';
});
