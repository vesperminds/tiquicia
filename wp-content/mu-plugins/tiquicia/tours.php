<?php defined('ABSPATH') or die;

add_action('init', function() {

	/* Tours */

	register_post_type('tour', [
		'labels' => [
			'name' => 'Tours',
			'singular_name' => 'Tour',
			'menu_name' => 'Tours',
			'name_admin_bar' => 'Tour',
			'add_new' => 'Add new',
			'add_new_item' => 'Add New Tour',
			'new_item' => 'New Tour',
			'edit_item' => 'Edit Tour',
			'view_item' => 'View Tour',
			'all_items' => 'All Tours',
			'search_items' => 'Search Tours',
			'parent_item_color' => 'Parent Tours:',
			'not_found' => 'No tours found.',
			'not_found_in_trash' => 'No tours found in Trash',
		],
		'description' => 'Tours custom post type for Tiquicia Tours.',
		'public' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-palmtree',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'editor',
			'thumbnail'
		],
		'rewrite' => [
			'slug' => 'tour'
		]
	]);

	/* Meta */

	VP_MetaBox::registerForCustomPostType('tour', [
		'id' => 'tour-information',
		'title' => 'Tour Information',
		'context' => 'normal',
		'priority' => 'default',

		'group-options' => [
			'videos' => [
				'label' => 'YouTube Videos'
			]
		],

		'fields' => [

			'duration' => [
				'label' => 'Duration',
				'type' => 'text',
				'note' => '50 ch.',
				'maxlength' => 50
			],

			'what-to-bring' => [
				'label' => 'What to bring',
				'type' => 'text',
				'note' => '500 ch.',
				'maxlength' => 500
			],

			'highlights' => [
				'label' => 'Highlights',
				'type' => 'text',
				'note' => '500 ch.',
				'maxlength' => 500
			],

			'youtube-ids' => [
				'label' => 'YouTube IDs',
				'type' => 'text',
				'note' => 'sep. by comma',
				'group' => 'videos'
			],

		]
	]);


	/* Tour types */

	register_taxonomy('tour_types', 'tour', [
		'labels' => [
			'name' => 'Tour Types',
			'singular_name' => 'Tour Type',
			'all_items' => 'All Tour Types',
			'edit_item' => 'Edit Tour Type',
			'view_item' => 'View Tour Type',
			'update_item' => 'Update Tour Type',
			'add_new_item' => 'Add New Tour Type',
			'new_item_name' => 'New Tour Type Name',
			'parent_item' => 'Parent Tour Type',
			'parent_item_colon' => 'Parent Tour Type:',
			'search_items' => 'Search Tour Types',
			'popular_items' => 'Popular Tour Types',
			'separate_items_with_commas' => 'Separate tour types with commas',
			'add_or_remove_items' => 'Add or remove tour types',
			'choose_from_most_used' => 'Choose from the most used tour types',
			'not_found' => 'No tour types found.',
			'menu_name' => 'Tour Types',
		],
		'public' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'hierarchical' => true,
		'rewrite' => [
			'slug' => 'tours',
			'hierarchical' => true
		]
	]);
	register_taxonomy_for_object_type('tour_types', 'tour');

});

/* Tours helper */

function tt_get_all_tours(array $opts = []) {

	$opts = array_merge([
		'type' => 'tour',
		'taxonomy' => 'tour_type',
		'thumbsize' => 'tiquicia-post-thumbnail-cover'
	], $opts);

	return vp_get_all($opts);

}
