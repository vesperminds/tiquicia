<?php defined('ABSPATH') or die;

add_action('init', function() {

	/* Tours */

	register_post_type('photo', [
		'labels' => [
			'name' => 'Photos',
			'singular_name' => 'Photo',
			'menu_name' => 'Gallery',
			'name_admin_bar' => 'Photo',
			'add_new' => 'Add new',
			'add_new_item' => 'Add New Photo',
			'new_item' => 'New Photo',
			'edit_item' => 'Edit Photo',
			'view_item' => 'View Photo',
			'all_items' => 'All Photos',
			'search_items' => 'Search Photos',
			'parent_item_color' => 'Parent Photos:',
			'not_found' => 'No photos found.',
			'not_found_in_trash' => 'No photos found in Trash',
		],
		'description' => 'Photos custom post type for Tiquicia Tours.',
		'public' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-camera',
		'capability_type' => 'page',
		'supports' => [
			'title',
			// 'editor',
			'thumbnail'
		],
		'rewrite' => [
			'slug' => 'photo'
		]
	]);


	add_action('add_meta_boxes_photo', function() {

		remove_meta_box('postimagediv', 'photo', 'side');
	    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'photo', 'normal', 'high');

	});


});
