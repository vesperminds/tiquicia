<?php defined('ABSPATH') or die;

add_action('admin_menu', function() {

	add_submenu_page(
		'edit.php?post_type=tour',
		'Tours in Front Page',
		'Front Page',
		'manage_options',
		'tour-home-options',
		function() {

			$tour_a = get_option('tiquicia_home_tour_a', null);
			$tour_b = get_option('tiquicia_home_tour_b', null);
			$tour_c = get_option('tiquicia_home_tour_c', null);
			$featured_tour = get_option('tiquicia_home_featured_tour', null);

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				update_option('tiquicia_home_tour_a', (int) $_POST['tour_a']);
				update_option('tiquicia_home_tour_b', (int) $_POST['tour_b']);
				update_option('tiquicia_home_tour_c', (int) $_POST['tour_c']);
				update_option('tiquicia_home_featured_tour', (int) $_POST['featured_tour']);

				$tour_a = (int) $_POST['tour_a'];
				$tour_b = (int) $_POST['tour_b'];
				$tour_c = (int) $_POST['tour_c'];
				$featured_tour = (int) $_POST['featured_tour'];

				$saved = true;

			}

			include __DIR__ . '/views/home-options.php';

		}
	);

});

add_action('admin_menu', function() {

	add_submenu_page(
		'edit.php?post_type=photo',
		'Gallery Videos',
		'Videos',
		'manage_options',
		'gallery-video-options',
		function() {

			$gallery_videos = get_option('tiquicia_gallery_videos', '');

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				update_option('tiquicia_gallery_videos', $_POST['tiquicia_gallery_videos']);
				$gallery_videos = $_POST['tiquicia_gallery_videos'];

				$saved = true;

			}

			include __DIR__ . '/views/gallery-video-options.php';

		}
	);

});
