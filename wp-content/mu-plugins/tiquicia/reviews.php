<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Tours */

    register_post_type('review', [
        'labels' => [
            'name' => 'Reviews',
            'singular_name' => 'Review',
            'menu_name' => 'Reviews',
            'name_admin_bar' => 'Review',
            'add_new' => 'Add new',
            'add_new_item' => 'Add New Review',
            'new_item' => 'New Review',
            'edit_item' => 'Edit Review',
            'view_item' => 'View Review',
            'all_items' => 'All Reviews',
            'search_items' => 'Search Reviews',
            'parent_item_color' => 'Parent Reviews:',
            'not_found' => 'No reviews found.',
            'not_found_in_trash' => 'No reviews found in Trash',
        ],
        'public' => true,
        'menu_position' => 30,
        'menu_icon' => 'dashicons-testimonial',
        'capability_type' => 'page',
        'supports' => [
            'editor',
            'page-attributes'
        ]
    ]);

    /* Meta */

    VP_MetaBox::registerForCustomPostType('review', [
        'id' => 'review-information',
        'title' => 'Review Information',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'author' => [
                'label' => 'Author',
                'type' => 'text',
                'maxlength' => 40,
            ],

            'date' => [
                'label' => 'Review Date',
                'type' => 'text',
                'maxlength' => 60,
            ],

        ]
    ]);

    add_filter('user_can_richedit', function($value) {

        global $post;

        if ($post->post_type === 'review') return false;

        return $value;

    });

    add_filter('manage_review_posts_columns', function($columns) {

        $cols = array_merge([
            'content' => 'Content',
            'review-author' => 'Author'
        ], $columns);

        unset($cols['title']);

        return $cols;

    });

    add_filter('manage_review_posts_custom_column', function($column) {

        global $post;

        $edit_url = vp_url("/wp-admin/post.php?post={$post->ID}&action=edit");

        switch ($column) {

            case 'content':
            echo "<a href=\"{$edit_url}\">{$post->post_content}</a>";
            break;

            case 'review-author':
            echo get_post_meta($post->ID, 'author', true);
            break;
        }

    });

});

/* Reviews helper */

function get_all_reviews() {

    $posts = [];
    $postsObj = new WP_Query([
        'posts_per_page' => '-1',
        'post_type' => 'review',
    ]);

    while ($postsObj->have_posts()) {
        $postsObj->the_post();

        $meta = array_merge([
            'author' => [''],
            'date' => ['']
        ], get_post_meta(get_the_ID()));

        $post = [
            'id' => get_the_ID(),
            'content' => strip_tags(get_the_content()),
            'author' => $meta['author'][0],
            'date' => $meta['date'][0]
        ];

        $posts[] = $post;
    }

    return $posts;

}
