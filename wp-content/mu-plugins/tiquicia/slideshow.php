<?php defined('ABSPATH') or die;

// Post type

add_action('init', function() {

	register_post_type('slide', [
		'labels' => [
			'name' => 'Slides',
			'singular_name' => 'Slide',
			'menu_name' => 'Slides',
			'name_admin_bar' => 'Slide',
			'add_new' => 'Add new',
			'add_new_item' => 'Add New Slide',
			'new_item' => 'New Slide',
			'edit_item' => 'Edit Slide',
			'view_item' => 'View Slide',
			'all_items' => 'All Slides',
			'search_items' => 'Search Slides',
			'parent_item_color' => 'Parent Slides:',
			'not_found' => 'No slides found.',
			'not_found_in_trash' => 'No slides found in Trash',
		],
		'description' => __('Slides custom post type for VesperPress Engine.'),
		'public' => false,
		'show_ui' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-format-gallery',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'thumbnail'
		]
	]);

	add_action('add_meta_boxes_slide', function() {

		remove_meta_box('postimagediv', 'slide', 'side');
	    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'slide', 'normal', 'high');

	});

	add_filter('user_can_richedit', function($value) {

		global $post;

		if ($post->post_type === 'slide') return false;

		return $value;

	});

	VP_MetaBox::registerForCustomPostType('slide', [
		'id' => 'slideshow',
		'title' => 'Slideshow',
		'context' => 'side',
		'priority' => 'high',
		'fields' => [

			'active' => [
				'label' => 'Active',
				'type' => 'boolean'
			],

			'bound-to' => [
				'label' => 'Show in',
				'type' => 'enum-fn',
				'values' => function() {

					$output = [];

					$output['Pages'] = [
						'@home' => 'Front Page'
					];

					/* Tours */

					$tours_raw = get_posts([
						'post_type' => 'tour',
						'posts_per_page' => -1,
						'orderby' => 'title'
					]);

					$tours = [];

					foreach ($tours_raw as $tour) {
						$tours['tour:' . $tour->ID] = $tour->post_title;
					}

					$output['Tours'] = $tours;

					/* Bye */

					return $output;

				}
			],

			'position' => [
				'label' => 'Priority',
				'type' => 'number',
				'required' => true,
				'default' => 1
			],

		]
	]);

	add_filter('manage_slide_posts_columns', function($columns) {

		return array_merge($columns, [
			'slide-active' => 'Active',
			'slide-bound-to-and-position' => 'Show in'
		]);

	});

	add_filter('manage_slide_posts_custom_column', function($column) {

		global $post;

		switch ($column) {

			case 'slide-active':

				$active = get_post_meta($post->ID, 'active', true);
				echo '<input disabled type="checkbox" ' . ($active ? 'checked' : '') . ' />';
				break;

			case 'slide-bound-to-and-position':

				$bound_to = get_post_meta($post->ID, 'bound-to', true);
				$position = get_post_meta($post->ID, 'position', true);

				if (!$bound_to) {
					echo '(loose)';
					break;
				}

				if ($bound_to === '@home') {
					echo 'Home Page';
				}
				elseif (strpos($bound_to, ':') !== false) {
					$term_args = explode(':', $bound_to, 2);
					$term = get_post($term_args[1]);
					echo $term->post_title;
				}
				else {
					echo '(unknown)';
				}

				echo '<br>&rarr; ' . 'Position' . ': ' . ($position ?: '(default)');

				break;

		}

	});

});

