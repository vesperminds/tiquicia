<?php

	$tours = tt_get_all_tours([ 'nothumb' => true ]);

	foreach ($tours as $tour) {
		 '<option value="' . $tour['id'] . '">' . $tour['title'] . '</option>';
	}

?>
<div class="wrap">

	<h1>Tours in Front Page</h1>

	<?php if (!empty($saved)): ?>
		<div id="message" class="updated notice notice-success is-dismissible below-h2">
			<p>
				Options saved.
				<a href="<?= vp_url('/') ?>">View</a>
			</p>
			<button type="button" class="notice-dismiss">
				<span class="screen-reader-text">Dismiss this notice.</span>
			</button>
		</div>
	<?php endif; ?>

	<form method="post" action="">

		<div style="float: right;">

			<span style="display: inline-block; background: #333; color: #fff; text-align: center; margin-top: 0.25em; padding: 0.25em 0; width: 2em;">A</span>
			<span style="display: inline-block; background: #333; color: #fff; text-align: center; margin-top: 0.25em; padding: 0.25em 0; width: 2em;">B</span><br>
			<span style="display: inline-block; background: #999; color: #fff; text-align: center; margin-top: 0.25em; padding: 0.25em 0; width: 2em;">&nbsp;</span>
			<span style="display: inline-block; background: #333; color: #fff; text-align: center; margin-top: 0.25em; padding: 0.25em 0; width: 2em;">C</span>

		</div>


		<p>
			Tour A:
			<select name="tour_a">
				<?php foreach ($tours as $tour): ?>
					<option <?= $tour_a == $tour['id'] ? 'checked' : '' ?> value="<?= $tour['id'] ?>"><?= $tour['title'] ?></option>
				<?php endforeach; ?>
			</select>
		</p>

		<p>
			Tour B:
			<select name="tour_b">
				<?php foreach ($tours as $tour): ?>
					<option <?= $tour_a == $tour['id'] ? 'checked' : '' ?> value="<?= $tour['id'] ?>"><?= $tour['title'] ?></option>
				<?php endforeach; ?>
			</select>
		</p>

		<p>
			Tour C:
			<select name="tour_c">
				<?php foreach ($tours as $tour): ?>
					<option <?= $tour_c == $tour['id'] ? 'checked' : '' ?> value="<?= $tour['id'] ?>"><?= $tour['title'] ?></option>
				<?php endforeach; ?>
			</select>
		</p>

		<hr class="clear">

		<p>
			Featured Tour:
			<select name="featured_tour">
				<?php foreach ($tours as $tour): ?>
					<option <?= $featured_tour == $tour['id'] ? 'checked' : '' ?> value="<?= $tour['id'] ?>"><?= $tour['title'] ?></option>
				<?php endforeach; ?>
			</select>
		</p>

		<hr class="clear">

		<button type="submit" class="button action">Apply</button>

	</form>

</div>