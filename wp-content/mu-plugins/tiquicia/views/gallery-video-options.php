<div class="wrap">

	<h1>Gallery Videos</h1>

	<?php if (!empty($saved)): ?>
		<div id="message" class="updated notice notice-success is-dismissible below-h2">
			<p>
				Options saved.
				<a href="<?= vp_url('/gallery') ?>">View</a>
			</p>
			<button type="button" class="notice-dismiss">
				<span class="screen-reader-text">Dismiss this notice.</span>
			</button>
		</div>
	<?php endif; ?>

	<form method="post" action="">

		<p>
			Youtube IDs:
			<input type="text" name="tiquicia_gallery_videos" value="<?= $gallery_videos ?>">
			<br>
			<em>Separated by commas.</em>
		</p>

		<hr class="clear">

		<button type="submit" class="button action">Apply</button>

	</form>

</div>
