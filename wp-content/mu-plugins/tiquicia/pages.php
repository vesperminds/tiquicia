<?php defined('ABSPATH') or die;

/* Meta */

VP_MetaBox::registerForCustomPostType('page', [
	'id' => 'page-information',
	'title' => 'Page Information',
	'context' => 'normal',
	'priority' => 'default',

	'group-options' => [ ],

	'fields' => [

		'headline' => [
			'label' => 'Headline',
			'type' => 'text',
			'note' => '160 ch.',
			'maxlength' => 160
		],

	]
]);