<?php defined('ABSPATH') or die;

/* Meta for themes */

function vp_fetch_meta($post_id = null) {

	$data = get_post_meta($post_id ?: get_the_ID());
	return new VP_Meta($data);

}

class VP_Meta {

	protected $data = [];

	public function __construct($data = null) {

		if (!empty($data) && is_array($data)) {
			$this->data = $data;
		}

	}

	public function has($key) {

		return isset($this->data[$key]);

	}

	public function get($key, $index = 0) {

		if (!$this->has($key)) {
			return null;
		}

		if (!isset($this->data[$key][$index])) {
			return null;
		}

		return $this->data[$key][$index];

	}

	public function any(array $keys = []) {

		if (empty($keys)) {
			return false;
		}

		$has = false;

		foreach ($keys as $key) {
			if ($this->has($key)) {
				$has = true;
				break;
			}
		}

		return $has;

	}

}

/* Metabox in CMS */

class VP_MetaBox {

	protected $_options;

	public function __construct($options) {

		if (empty($options['id'])) {
			throw new \Exception('Missing id');
		}

		if (empty($options['title'])) {
			throw new \Exception('Missing title');
		}

		if (empty($options['screen'])) {
			throw new \Exception('Missing screen context');
		}

		if (empty($options['fields'])) {
			throw new \Exception('Missing fields');
		}

		$this->_options = $options;

	}

	public function initialize() {

		add_action('add_meta_boxes', [ $this, '_add' ]);
		add_action('save_post', [ $this, '_save' ], 10, 2);

	}

	public function _add() {

		add_meta_box(
			$this->option('id'),
			$this->option('title'),
			[$this, '_render'],
			$this->option('screen'),
			$this->option('context') ?: 'advanced',
			$this->option('priority') ?: 'default',
			$this->option('arguments') ?: null
		);

	}

	public function _render($post, $wp_metabox) {

		$box_id = $this->option('id');

		$_defaultGroupName = '_default';
		$groups = $this->_regroup($this->option('fields'), $_defaultGroupName);

		$groupOptions = $this->option('group-options') ?: [];

		if (!empty($post->ID)) {
			$data = $this->_get_custom_fields($post->ID);
		}

		include __DIR__ . '/views/metabox.php';

	}

	public function _save($post_id) {

		global $post;

		$box_id = $this->option('id');

		if (
			!isset($_POST['meta_box_nonce-' . $box_id]) ||
			!wp_verify_nonce($_POST['meta_box_nonce-' . $box_id], $box_id)
		) {
			return $post_id;
		}

		if (
			(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
			isset($_REQUEST['bulk_edit'])
		) {
			return $post_id;
		}

		if (isset($post->post_type) && $post->post_type == 'revision') {
			return $post_id;
		}

		if (!current_user_can('edit_post', $post->ID)) {
			return $post_id;
		}

		if (empty($_POST['meta_box'][$box_id])) {
			return $post_id;
		}

		$current_meta = $this->_get_custom_fields($post->ID);

		foreach ($_POST['meta_box'][$box_id] as $field => $value) {

			if (!empty($value)) {
				update_post_meta($post->ID, $field, $value);
			}
			elseif (empty($value) && !empty($current_meta[$field])) {
				delete_post_meta($post->ID, $field);
			}

		}

		return $post_id;

	}

	// Data

	public function _get_custom_fields($post_id) {

		$meta = get_post_custom($post_id);
		$field_names = array_keys($this->option('fields'));

		if (empty($meta)) {
			return null;
		}

		$output = [];

		foreach ($meta as $field => $values) {

			if (!in_array($field, $field_names)) {
				continue;
			}

			if (!isset($values[0])) {
				continue;
			}

			$output[$field] = $values[0];

		}

		return $output;

	}

	// Transformations

	public function _regroup($fields, $defaultGroup = '_default') {

		$output = [[
			'group' => $defaultGroup,
			'fields' => []
		]];
		$map = [ $defaultGroup => 0 ];

		foreach ($fields as $id => $field) {

			$group = !empty($field['group']) ? $field['group'] : $defaultGroup;

			if (!isset($map[$group])) {
				$output[] = [
					'group' => $group,
					'fields' => []
				];
				$index = count($output) - 1;
				$map[$group] = $index;
			}
			else {
				$index = $map[$group];
			}

			$output[$index]['fields'][$id] = $field;

		}

		return $output;

	}

	// Options interface

	public function option($key) {

		return isset($this->_options[$key]) ? $this->_options[$key] : null;

	}

	// (static) Factory interface

	protected static $metaboxes = [];
	protected static $idCounter = 1000;

	public static function register($options) {

		if (empty($options['screen'])) {
			throw new \Exception('Missing screen context');
		}

		if (empty($options['id'])) {
			$options['id'] = static::generateId($options);
		}

		if (empty($options['title'])) {
			$options['title'] = 'MetaBox `' . $options['id'] . '`';
		}

		static::$metaboxes[$options['id']] = new static($options);
		static::$metaboxes[$options['id']]->initialize();

		return $options['id'];

	}

	public static function registerForPost($options) {

		return static::register(array_merge($options, [ 'screen' => 'post' ]));

	}

	public static function registerForPage($options) {

		return static::register(array_merge($options, [ 'screen' => 'page' ]));

	}

	public static function registerForDashboard($options) {

		return static::register(array_merge($options, [ 'screen' => 'dashboard' ]));

	}

	public static function registerForAttachment($options) {

		return static::register(array_merge($options, [ 'screen' => 'attachment' ]));

	}

	public static function registerForCustomPostType($custom_post_type, $options) {

		return static::register(array_merge($options, [ 'screen' => $custom_post_type ]));

	}

	public static function registerForComment($options) {

		return static::register(array_merge($options, [ 'screen' => 'comment' ]));

	}

	protected static function generateId($options) {

		if (empty($options) || empty($options['screen'])) {
			throw new \UnexpectedValueException('Empty options[screen]');
		}

		static::$idCounter += 1;

		$prefix = preg_replace('/[^a-z0-9]/', '', strtolower($options['screen']));
		$mainId = '';
		$suffix = static::$idCounter;

		if (!empty($options['title'])) {
			$mainId .= preg_replace('/[^a-z0-9]/', '-', strtolower($options['title']));
		}

		$construct = !empty($mainId) ? [$prefix, $mainId, $suffix] : [$prefix, $suffix];

		return implode('-', $construct);

	}

}
