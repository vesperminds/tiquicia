<?php defined('ABSPATH') or die;

function vp_get_all(array $opts = []) {

	$opts = array_merge([
		'limit' => -1,
		'page' => null,
		'orderby' => 'title',
		'order' => 'asc',
		'type' => 'post',
		'taxonomy' => 'category',
		'thumbsize' => 'thumb',
	], $opts);

	$args = [
		'posts_per_page' => $opts['limit'],
		'post_type' => $opts['type'],
		'orderby' => $opts['orderby'],
		'order' => $opts['order']
	];

	if (!empty($opts['taxonomy']) && !empty($opts['taxonomyslug'])) {

		$args['tax_query'] = [
			[
				'taxonomy' => $opts['taxonomy'],
				'field' => 'slug',
				'terms' => $opts['taxonomyslug']
			]
		];

		// TODO: Add negation if needed (operator NOT IN)

	}

	if (!empty($opts['page'])) {
		$args['paged'] = $opts['page'];
	}

	if (!empty($opts['include'])) {

		if (!is_array($opts['include'])) {
			$opts['include'] = [ $opts['include'] ];
		}

		$args['post__in'] = $opts['include'];

	}
	elseif (!empty($opts['exclude'])) {

		if (!is_array($opts['exclude'])) {
			$opts['exclude'] = [ $opts['exclude'] ];
		}

		$args['post__not_in'] = $opts['exclude'];

	}

	$postsObj = new WP_Query($args);

	$posts = [];

	if ($postsObj->have_posts()) {

		while ($postsObj->have_posts()) {

			$postsObj->the_post();

			$post = [
				'id' => get_the_ID(),
				'title' => get_the_title(),
				'permalink' => get_the_permalink()
			];

			$thumbnail_id = false;

			if (empty($opts['nothumb'])) {

				$thumb = '';
				$thumbnail_id = get_post_thumbnail_id(get_the_ID());

				if (!empty($thumbnail_id)) {
					$image = wp_get_attachment_image_src($thumbnail_id, $opts['thumbsize']);

					if (!empty($image)) {
						$thumb = $image[0];
					}
				}

				$post['thumb'] = $thumb;

			}

			if (!empty($opts['extraimage'])) {

				$extra = '';

				if ($thumbnail_id === false) {
					$thumbnail_id = get_post_thumbnail_id(get_the_ID());
				}

				if (!empty($thumbnail_id)) {
					$image = wp_get_attachment_image_src($thumbnail_id, $opts['extraimage']);

					if (!empty($image)) {
						$extra = $image[0];
					}
				}

				$post['extraimage'] = $extra;

			}

			$posts[] = $post;

		}

		wp_reset_postdata();

	}

	return $posts;

}