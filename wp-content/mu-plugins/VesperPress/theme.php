<?php defined('ABSPATH') or die;

/* Blog URL helper */

function vp_url($path = '/') {

	return get_site_url(null, $path);

}

/* Theme files helper functions */

function vpth_path($path = '/') {

	return get_stylesheet_directory_uri() . $path;

}

function vpth_version($path = '/') {

	$file = get_stylesheet_directory() . $path;

	if (is_readable($file) && is_file($file)) {

		$ver = (string) filemtime($file);

		if (!$ver) {
			$ver = date('YmdH');
		}

	}
	else {
		$ver = date('YmdH');
	}

	return $ver;

}