<?php defined('ABSPATH') or die; ?>
<style type="text/css">

#<?= $box_id ?> label.for-input {
	width: 32.5%;
	display: inline-block;
	text-align: right;
	margin-right: 1em;
	color: #333333;
}

#<?= $box_id ?> label.for-textarea {
	padding-left: 32.5%;
	padding-left: calc(32.5% + 1.2em);
	display: block;
	color: #333333;
}

#<?= $box_id ?> label span.label-note {
	color: #aaaaaa;
	font-size: 0.8em;
}

#<?= $box_id ?> .group-title {
	font-weight: bold;
	margin-top: 2em;
	padding: 1.5em 25px;
	font-size: 1em;
	border-top: 1px solid #cccccc;
	color: #999999;
}

#<?= $box_id ?> input[type=text],
#<?= $box_id ?> input[type=password],
#<?= $box_id ?> input[type=number],
#<?= $box_id ?> input[type=email] {
	width: 52.5%;
}


#<?= $box_id ?> textarea {
	margin-left: 32.5%;
	margin-left: calc(32.5% + 1.2em);
	display: block;
	width: 52.5%;
	height: 6em;
	resize: none;
}

#<?= $box_id ?> .meta_box-group .collapsible-button:focus,
#<?= $box_id ?> .meta_box-group .collapsible-button:active {
	outline: none;
	box-shadow: none;
	color: #000000;
}

#<?= $box_id ?> .meta_box-group .collapsible-button {
	display: inline-block;
}

#<?= $box_id ?> .meta_box-group .collapsible-button:before {
	font-family: dashicons;
	text-decoration: inherit;
	font-weight: 400;
	font-style: normal;
	line-height: 1.2em;
	content: "\f347";
	display: inline-block;
	color: #888888;
	text-decoration: none;
	padding: 0 0.5em;
	text-align: center;
	position: relative;
	top: 1px;
}

#<?= $box_id ?> .meta_box-group.meta_box-collapsed .collapsible-button:before {
	content: "\f345";
}

#<?= $box_id ?> .meta_box-group.meta_box-collapsed .group-title {
	padding-bottom: 0;
}

#<?= $box_id ?> .meta_box-group.meta_box-collapsed .group-content {
	display: none;
}

#<?= $box_id ?> select {
	max-width: 55%;
}

</style>

<?php wp_nonce_field($box_id, 'meta_box_nonce-' . $box_id) ?>

<?php foreach ($groups as $group): ?>

<div class="meta_box-group <?=
!empty($groupOptions[$group['group']]['collapsed-initially']) ?
'meta_box-collapsed' : ''
?>">

	<?php if ($group['group'] != $_defaultGroupName): ?>
	<p class="group-title"
	<?php if (!empty($groupOptions[$group['group']]['collapsible'])): ?>
	onclick="jQuery(this).closest('.meta_box-group').toggleClass('meta_box-collapsed'); return false"
	<?php endif; ?>
	>
		<?php if (!empty($groupOptions[$group['group']]['collapsible'])): ?>
		<a class="collapsible-button" href="#"></a>
		<?php endif; ?>
		<?= !empty($groupOptions[$group['group']]['label']) ?
		$groupOptions[$group['group']]['label'] :
		$group['group'] ?>
	</p>
	<?php endif; ?>

	<div class="group-content">

	<?php foreach ($group['fields'] as $id => $field): ?>
	<?php
		$htmlId = $box_id . '-' . $id;
	?>

		<p>
		<?php if (in_array($field['type'], ['text', 'number', 'email'])): ?>

			<label class="for-input" for="<?= $htmlId ?>">
				<?= $field['label'] ?>
				<?php if (!empty($field['note'])): ?>
				<span class="label-note">(<?= $field['note'] ?>)</span>
				<?php endif; ?>
			</label>
			<input type="<?= $field['type'] ?>" id="<?= $htmlId ?>" <?=
				isset($data[$id]) ? 'value="' . htmlentities($data[$id]) . '"' : ''
			?> <?=
				!isset($data[$id]) && isset($field['default']) ? 'value="' . htmlentities($field['default']) . '"' : ''
			?> <?=
				!empty($field['maxlength']) ? 'maxlength="' . htmlentities($field['maxlength']) . '"' : ''
			?> name="meta_box[<?= $box_id ?>][<?= $id ?>]" />

		<?php elseif ($field['type'] === 'longtext'): ?>

			<label class="for-textarea" for="<?= $htmlId ?>">
				<?= $field['label'] ?>
				<?php if (!empty($field['note'])): ?>
				<span class="label-note">(<?= $field['note'] ?>)</span>
				<?php endif; ?>
			</label>
			<textarea id="<?= $htmlId ?>" name="meta_box[<?= $box_id ?>][<?= $id ?>]"><?=
				!empty($data[$id]) ? htmlentities($data[$id]) : ''
			?></textarea>

		<?php elseif ($field['type'] === 'boolean'): ?>

			<label class="for-input" for="<?= $htmlId ?>">
				<?= $field['label'] ?>
				<?php if (!empty($field['note'])): ?>
				<span class="label-note"><?= $field['note'] ?></span>
				<?php endif; ?>
			</label>
			<input type="checkbox" id="<?= $htmlId ?>" <?=
				!empty($data[$id]) && (int) $data[$id] == 1 ? 'checked="checked"' : ''
			?> name="meta_box[<?= $box_id ?>][<?= $id ?>]" value="1" />

		<?php elseif (
			$field['type'] === 'enum' ||
			$field['type'] === 'enum-fn'
		): ?>

			<?php
				$values = $field['type'] === 'enum-fn' ? $field['values']() : $field['values'];
			?>

			<label class="for-input" for="<?= $htmlId ?>">
				<?= $field['label'] ?>
				<?php if (!empty($field['note'])): ?>
				<span class="label-note"><?= $field['note'] ?></span>
				<?php endif; ?>
			</label>
			<select id="<?= $htmlId ?>" name="meta_box[<?= $box_id ?>][<?= $id ?>]">
				<?php if (empty($field['required'])): ?>
				<option value=""><i>(not set)</i></option>
				<?php endif; ?>
				<?php foreach ($values as $optValue => $optLabel): ?>

				<?php if (!is_array($optLabel)): ?>

					<option value="<?= $optValue ?>" <?=
						!empty($data[$id]) && $data[$id] == $optValue ? 'selected="selected"' : ''
					?>><?= $optLabel ?></option>

				<?php else: //due to lack of planning, things below look reversed ?>

					<optgroup label="<?= $optValue ?>">
						<?php foreach ($optLabel as $subOptValue => $subOptLabel): ?>
						<option value="<?= $subOptValue ?>" <?=
							!empty($data[$id]) && $data[$id] == $subOptValue ? 'selected="selected"' : ''
						?>><?= $subOptLabel ?></option>
						<?php endforeach; ?>
					</optgroup>

				<?php endif; ?>

				<?php endforeach; ?>
			</select>

		<?php endif; ?>
		</p>

	<?php endforeach; ?>

	</div>

</div>
<?php endforeach; ?>